+++
title = "主页"
date = 2024-01-31
page_template="page.html"

[extra]
title = ""

interests = [
  "Livres",
  "Musique",
]

[[extra.education.courses]]
  degree = "理学硕士: 计算机科学"
  institution = "杜克大学 Duke University"
  time = "2025.05"
  GPA = '4.0/4.0'
  Option = ''
  Minor = ''

[[extra.education.courses]]
  degree = "理学学士: 计算机科学与工程"
  institution = "俄亥俄州立大学 The Ohio State University"
  time = '2023.05'
  GPA = '4.0/4.0'
  Option = '人工智能'
  Minor = '数学'
  Honors = ["Dean's List","Summa Cum Laude 荣誉毕业生"]
  


#[[extra.avatar_icons]]
#  icon = "github"
#  link = "https://github.com/adfaure"
[[extra.avatar_icons]]
  icon = "gitlab"
  link = "https://coursework.cs.duke.edu/hg163"
[[extra.avatar_icons]]
  icon = "linkedin"
  link = "https://www.linkedin.com/in/huanli-gong-946903293/"
+++

我叫龚桓立，是[杜克大学](https://duke.edu/)的一名硕士生，主修计算机科学。在进入杜克大学之前，我在[俄亥俄州立大学](https://www.osu.edu/) 获得了计算机科学与工程学士学位，GPA 为 4.0。除了保持成绩，我还积极寻求学术和专业机会。我曾在 [OSU NLP group](https://u.osu.edu/ihudas/people/) 工作并发表过论文。我目前的兴趣是 [kaggle](https://www.kaggle.com/huanligong) 并成为竞赛专家(排名3133/223,016)。现在我正在寻找暑期实习！