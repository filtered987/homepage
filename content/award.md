+++
title = "Award"
date = 2024-01-31

[extra]

[[extra.list]]
  date='2024-01-22'
  name = "LLM - Detect AI Generated Text"
  prize='Bronze Medal'
  top=8
  url = "https://kaggle.com/competitions/llm-detect-ai-generated-text"

[[extra.list]]
  date='2023-11-17'
  name = "Google - Fast or Slow? Predict AI Model Runtime"
  prize='Bronze Medal'
  top=11
  url = "https://www.kaggle.com/competitions/predict-ai-model-runtime"

[[extra.list]]
  date='2018-2019; 2019-2020'
  name = "Academic Merit Scholarship from Beijing Jiaotong University"

+++

