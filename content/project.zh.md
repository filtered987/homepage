+++
title = "项目"
date = 2024-01-31

[extra]

[[extra.list]]
  name = "强化学习社交项目"
  time = "2022.05 ~ 2022.08"
  location = ''
  # role = "Mentored by Rui Ding from Microsoft (China) Co"
  role = ''
  summary = '对社交网络上的新闻传播进行建模: 包括利用Multi-Agent Deep Deterministic Policy Gradient (MADDPG) 来解决图上的动态过程，并通使用MARWIL实现离线强化学习。'
  details = [
  ]
[[extra.list]]
  name = "Ruby on Rails 项目: 基于网页的演讲评估程序"
  time = "2021.09 ~ 2021.12"
  location = ''
  role = ''
  summary = '实施网络应用程序，简化听众对演讲评价的收集、整理和分析工作。教师可以管理用户、演讲和团队，学生可以提交和查看反馈。扩展功能包括身份验证、管理员仪表板、用户警报、输出格式化、PostgreSQL 与 Heroku。'
  details = [
  ]
[[extra.list]]
  name = "quandary 语言解释器"
  time = "2022.01 ~ 2022.05"
  location = ''
  role = ''
  summary = '使用Java实现了一个 Quandary 语言解释器，具有所有语言特点，包括函数调用、突变、堆和并发性。'
  details = [
  ]
#[[extra.experiences]]
#  name = "Ruby on Rails Project: Web-based Presentations Evaluation Application"
#  time = "2021.09 ~ 2021.12"
#  location = ''
#  role = ''
#  summary = 'Implemented a web application would streamline the collection, collation, and analysis of audience evaluations of presentations. Instructors can manage users, presentations, and teams, and students can submit and view feedbacks. Extended features include authentication, admin dashboard, user alerts, output formatting, PostgreSQL with Heroku.'
#  details = []

+++
