+++
title = "科研"
date = 2024-01-31

[extra]

[[extra.list]]
  name = "Amazon's Alexa Prize TaskBot Challenge 2"
  time = "2023.01 ~ 2023.09"
  location = ''
  role = "俄亥俄州立大学队员"
  summary = '领导自然语言理解（NLU）分队，负责ASR纠正、意图分类、问题分类；并参与用户交互分队。'
  details = [
    "针对对话场景设计并实施了基于知识集的ASR纠正方法，并将其与各种语言模型进行了性能评估。",
    "调试并改进了意图分类器、问题分类器以及其他模块。",
    "监控用户评级和反馈，并根据用户兴趣选择TaskBot推荐的任务。"
  ]

[[extra.list]]
  name = "自动语音识别 (ASR) 错误纠正项目"
  time = "2022.05 ~ 2022.12"
  location = ''
  role = "实验室主任 (Prof. Huan Sun) 的研究助理"
  summary = "提出一种纠正ASR转录错误的方法，并将它运用于TaskBot Challenge 2。"
  details = [
    "实施了一个通过模拟噪声自动生成ASR错误的管道，并利用它获取噪声版本的数据集。",
    "出了一种提高语言模型在ASR错误纠正任务上性能的方法，即预训练过程中模型除了从上下文中获取词义外，还被注入发音信息的编码。",
    "测试用于 ASR 纠错的语言模型的性能。"
  ]

[[extra.list]]
  name = "教育领域深度问题的数据集"
  time = "2021.08 ~ 2022.09"
  location = ''
  role = "项目领导人"
  summary = "发布一篇关于为语言模型生成深度问题的数据集的论文在NLP领域顶级会议（COLING）上。"
  details = [
    "召集并领导团队从可汗学院中提取、转述并注释的真实问题和答案，作为名为KHANQ的数据集。",
    "对包括 GPT、BART 和T5 在内的 SOTA 文本生成模型进行了自动（BLEU、METEOR、ROUGE-L）和人工评估，总结了模型生成深度问题的所面临的挑战。",
  ]

+++
