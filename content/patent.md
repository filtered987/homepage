+++
title = "Patent"
date = 2024-01-31

[extra]

[[extra.list]]
  authors = ['Huanli Gong']
  date='2022-05-17'
  name = "Router capable of displaying network information"
  number =  "216565201"
  kind = 'U'
  office='CH'
  url = "https://patentscope.wipo.int/search/en/detail.jsf?docId=CN362221752&_cid=P12-L7L42O-92793-1"


[[extra.list]]
  authors = ['Huanli Gong']
  date='2021-09-07'
  name = "Four-way responder based on RS trigger"
  number =  "214151904"
  kind = 'U'
  office='CH'
  url = "https://patentscope.wipo.int/search/en/detail.jsf?docId=CN336288989&_cid=P12-L7L44A-92914-1"

[[extra.list]]
  authors = ['Congjiang Liu','Huanli Gong']
  date='2021-04-27'
  name = "Automatic gain and linear control device and method in radio frequency power amplifier"
  number =  "112713861"
  kind = 'A'
  office='CH'
  url = "https://patentscope.wipo.int/search/en/detail.jsf?docId=CN323586272&_cid=P21-L7L1FD-72950-1"

+++

