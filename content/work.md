+++
title = "Work"
date = 2024-01-31

[extra]

[[extra.list]]
  name = "Fujian Cooby Information Technology Co., Ltd."
  time = "2021.08 ~ 2021.09"
  location = 'Fuzhou, China'
  role = "Software Engineer Intern"
  summary = ''
  details = [
    "Developed the home page API on PC and mobile for the official website of China Federation Of Overseas Chinese Entrepreneurs (CFOCE) while prioritizing scalability and high responsiveness.",
    "Implemented the ability to view reviewers' logs for Chinese Headline New Media (CHNM).",
  ]

[[extra.list]]
  name = "Fujian Santone Information Technology Co., Ltd."
  time = "2019.07 ~ 2019.08"
  location = 'Xiamen, China'
  role = "Signal Processing Engineer Intern"
  summary = ""
  details = [
    "Designed the GUI for the Digital Pre-Distortion (DPD) program by analyzing user needs and translating them into core requirements, building wireframes, creating interactive prototypes, and evaluating the UI design.",
    "Created and executed test plans for the DPD project along with designing and developing automated testing frameworks to validate functionality and ensure system robustness. ",
  ]

+++
